<?php 
class Init { 
    private $db_host='localhost';
	private $db_name='my.db';
	private $db_user='root';
	private $db_pass='';
	private $db_table='test';
	private $results=['normal', 'illegal', 'failed', 'success'];

    function __construct() {
       create();
       fill();
    }

    private function create() {
    	# соединение с базой
    	$link = mysqli_connect($db_host,$db_user,$db_pass);
    	if (!$link) {
           die('Ошибка соединения: ' . mysqli_error());
	    }
        mysqli_select_db($link, $db_name) or die(mysql_error());

        # создание базы данных test
		mysqli_query($link, "CREATE TABLE test (
		  id INT AUTO_INCREMENT PRIMARY KEY,
		  script_name CHAR(25),
		  start_time INT,
		  end_time INT,
		  result CHAR
		)") Or die(mysqli_error());

		# завершение соединения
		mysqli_close ();
   }

   private function fill() {
   		# соединение с базой
       	$link = mysqli_connect($db_host,$db_user,$db_pass);
       	if (!$link) {
           die('Ошибка соединения: ' . mysqli_error());
	    }
	    mysqli_select_db($link,$db_name);

	    # генерация значений, (для генерации строки добавлена вспомогательная функция)	
	    $start_time = random_int(0, 256);
	    $end_time = random_int(0, 256);
	    $script_name = rand_str();
	    $result = $results[random_int(0, 4)];

	    # наполнение таблицы
	    $row = mysqli_query($link, "INSERT INTO ".$db_table." (`script_name`, `start_time`, `end_time`, `result`) VALUES ('".$start_time."','".$end_time."','".$script_name."','".$result."')");

	    # проверка добавления в базу
	    if ($row){
	        echo "Информация занесена в базу данных";
	    }else{
	        echo "Информация не занесена в базу данных";
	    }

	    # завершение соединения
	    mysqli_close ();
   }

   public function get() {
   		# соединение с базой
   		$link = mysqli_connect($db_host,$db_user,$db_pass);
   		if (!$link) {
           die('Ошибка соединения: ' . mysqli_error());
	    }
	    mysqli_select_db($link,$db_name);
		$query = "select * from test where result like 'success' or result like 'normal'";

		# вывод данных
		$result = $link->query($query);
		while($row = mysqli_fetch_array($result)) 
		{
			echo "script_name:".$row['script_name']."<br>";
			echo "start_time:".$row ['start_time']."<br>";
			echo "end_time:".$row ['end_time']."<br>";
			echo "result:".$row ['result']."<br>";
		}
   }

   private rand_str(){
	   	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $str = '';
	    for ($i = 0; $i <= 25; $i++) {
	        $str .= $characters[mt_rand (0, strlen ($characters) - 1)];
	    }
	    return $str;
   }
} 

$a = new Init; 
?> 