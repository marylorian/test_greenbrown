# README #

## This is a test task for work in GreenBrown company

### Задача №1.
Написать класс init, состоящий из 3 методов:

- create()

доступен только для методов класса

создает таблицу test, содержащую 5 полей:
id целое, автоинкрементарное
script_name строковое, длиной 25 символов
start_time целое
end_time целое
result один вариант из 'normal', 'illegal', 'failed', 'success'
- fill()
доступен только для методов класса
заполняет таблицу случайными данными
- get()
доступен извне класса
выбирает из таблицы test, данные по критерию: result среди значений 'normal' и 'success'
В конструкторе выполняются методы create и fill

Весь код должен быть прокомментирован.




### Задача №2.
Знания MySQL + оптимизировать запросы

Имеется 3 таблицы: info, data, link, есть запрос для получения данных:

select * from data,link,info where link.info_id = info.id and link.data_id = data.id

предложить варианты оптимизации:
таблиц
запроса.
Запросы для создания таблиц:

CREATE TABLE info (
id int(11) NOT NULL auto_increment,
name varchar(255) default NULL,
desc text default NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

CREATE TABLE data (
id int(11) NOT NULL auto_increment,
date date default NULL,
value INT(11) default NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

CREATE TABLE link (
data_id int(11) NOT NULL,
info_id int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;