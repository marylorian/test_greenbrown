import sqlite3
import random
from random import choice
from string import ascii_letters

class init:
    def __init__(self):
    	# соединение с базой
        conn = sqlite3.connect('my.db')
        # создание курсора
        cur = conn.cursor()

        self.__create(cur)
        self.__fill(conn, cur)

    def __create(self, cur):
		# создание базы данных test
        cur.execute('CREATE TABLE IF NOT EXISTS test (id int auto_increment primary key,script_name varchar(25),start_time int, end_time int, result varchar)')

    def __fill(self, conn, cur):
        results = [ 'normal', 'illegal', 'failed', 'success' ]
        sc_name = ''.join(choice(ascii_letters) for i in range(25))
        st_time = random.randint(0, 256)
        e_time = random.randint(0, 256)
        res_number = random.randint(0, 3)

		# наполнение таблицы
        cur.execute('''
        	INSERT INTO test (script_name, start_time, end_time, result) 
        	VALUES ('%s','%d','%d','%s')'''
        	%(sc_name, st_time, e_time, results[res_number]))

		# подтверждение отправки данных в базу
        conn.commit()

        # завершение соединения
        cur.close()
        conn.close()

    def get(self):
    	# соединяемся заново, так как после инициализации закрываем соединение
        conn = sqlite3.connect('my.db')
        cur = conn.cursor()

    	# выборка из базы только тех данных, где result == 'normal'||'success'
        cur.execute('''
        	SELECT *
        	FROM test 
        	WHERE result LIKE ('normal') 
        	OR result LIKE ('success') 
        	''')

        # вывод отобранных данных на печать в цикле
        row = cur.fetchone()
        while row is not None:
            print("id "+str(row[0])+" script "+str(row[1])+" st "+str(row[2])+" end "+str(row[3])+" result "+str(row[4]))
            row = cur.fetchone()

        # завершаем соединение
        conn.close()

a = init()
a.get()